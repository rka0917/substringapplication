package com.example.rk.sub_stringapplication

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import org.jetbrains.anko.doAsync
import java.io.BufferedReader
import java.net.HttpURLConnection
import java.net.URL

import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.toast

class MainActivity : AppCompatActivity() {
    var dataDownloaded : Boolean = false
    var dataTrie = TrieNode("")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        progressBar.visibility = View.VISIBLE

        //Get data from server and populate the Trie
        doAsync {
            var line : String
            val connection = URL("http://runeberg.org/words/ss100.txt").openConnection() as HttpURLConnection
            try{
                connection.connect()
                val reader : BufferedReader = BufferedReader(connection.inputStream.reader(Charsets.ISO_8859_1))
                val iterator = reader.lineSequence().iterator()
                while (iterator.hasNext()){
                    line = iterator.next().toLowerCase()
                    dataTrie.add(line)
                }
                Log.d("NetworkTag", "We are done with downloading data and building trie")
            } finally {
                connection.disconnect()
            }
            //We are done with setup.
            dataDownloaded = true
            progressBar.visibility = View.GONE
        }

        //Set edittext to fetch all words when user presses enter
        inputEditText.setOnEditorActionListener(){_, actionId, _ ->
            if(actionId == EditorInfo.IME_ACTION_DONE){
                //Hide keyboard
                val imm :InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(this.currentFocus.windowToken, 0)

                //Empty the Listview before we get the new results
                var adapter = ArrayAdapter<String>(this@MainActivity, android.R.layout.simple_list_item_1, emptyArray())
                resultListView.adapter = adapter

                if(!dataDownloaded)
                    toast("Data has not finished downloaded yet!")
                else{
                    val inputWord = inputEditText.text.toString().toLowerCase()

                    if(inputWord.contains(" ") || inputWord.isNullOrEmpty())
                        toast("Please enter one word!")
                    else {
                        val wordList : ArrayList<String>? = getWordList(inputWord)
                        if(wordList != null){
                            Log.d("ASYNC", wordList?.size.toString())
                            adapter = ArrayAdapter<String>(this@MainActivity, android.R.layout.simple_list_item_1, wordList)
                            resultListView.adapter = adapter
                        }
                    }
                }
                true
            } else {
                false
            }
        }
    }

    /*Get all words from a substring */
    fun getWordList(substring : String) : ArrayList<String>?{
        val trie : TrieNode? = dataTrie.get(substring)
        if(trie == null)
            return null

        val words : ArrayList<String> = trie.getWordsFromSubword(substring)
        return words
    }


}
