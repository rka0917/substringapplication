package com.example.rk.sub_stringapplication

import android.util.Log

class TrieNode (inChar : String){
    //Every node contains a map with letters as keys and child node as value.
    //They also have a string that shows the corresponding string and a boolean that says if said string is a word

    val children = mutableMapOf<String, TrieNode>()
    var char : String
    var isWord : Boolean

    init{
        char = inChar
        isWord = false
    }

    //Add word to Trie structure
    fun add(word : String){
        val letter : String = word.take(1)

        if(word.length == 1){
            val newNode : TrieNode = TrieNode(letter)
            newNode.isWord = true
            children.put(letter, newNode)
        } else {
            if(children.containsKey(letter)){
                children.get(letter)!!.add(word.drop(1))
            } else {
                val newNode : TrieNode = TrieNode(letter)
                children.put(letter, newNode)
                newNode.add(word.drop(1))
            }
        }
    }

    //Get TrieNode corresponding to input word
    fun get(word : String) : TrieNode? {
        if(word.length == 0){
            return this
        }

        val letter : String = word.take(1)
        if (!children.containsKey(letter))
            return null

        return children.get(letter)?.get(word.drop(1))
    }

    //Return all substring found from trie
    fun getWordsFromSubword(substring : String): ArrayList<String> {
        val wordList = arrayListOf<String>()
        if(this.children.isEmpty()){
            wordList.add(substring)
            return wordList
        }

        //If the node we are at is the end of a word, add the entire word to the word list
        if(this.isWord)
            wordList.add(substring)

        //For all childpaths, return all words
        this.children.forEach {
            val resultList = it.value.getWordsFromSubword(substring + it.key)
            wordList.addAll(resultList)
        }
        return wordList
    }

}
